package mt.app.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import bottomtab.NiceTabLayout;
import mt.app.model.PagerItems;

/**
 * Created by talib on 10/19/2016.
 */

//Pager Adapter start
public class BottomPagerAdapter extends FragmentPagerAdapter implements NiceTabLayout.IconTabProvider {

    private final List<Fragment> mFragments = new ArrayList<>();
    public List<PagerItems> mTabs;
    private boolean mIosStyleIcon = false;



    public BottomPagerAdapter(FragmentManager fm, List<PagerItems> mTabs) {
        super(fm);
        this.mTabs = mTabs;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mTabs.get(position).getTitle();
    }

    @Override
    public int getPageIconResId(int position) {
        return mIosStyleIcon ? mTabs.get(position).getIosIconResId() : mTabs.get(position).getAndroidIconResId();
    }


    public void addFragment(Fragment fragment) {
        mFragments.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }




}
//Pager adapter end