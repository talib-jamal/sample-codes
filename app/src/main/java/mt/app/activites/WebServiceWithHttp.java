package mt.app.activites;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.NotSerializableException;
import java.util.HashMap;

import mt.app.CheckInternet;
import mt.app.R;
import mt.app.basicfunctions.basic.MyMethods;

public class WebServiceWithHttp extends AppCompatActivity implements View.OnClickListener{

    private Activity activity;
    private MyMethods myMethods;
    private ProgressDialog progressDialog;
    private Button get, post;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service_with_http);

        activity = this;
        myMethods = MyMethods.getInsance();
        get = (Button)findViewById(R.id.get);
        get.setOnClickListener(this);

        post = (Button)findViewById(R.id.post);
        post.setOnClickListener(this);




    }
    //Oncreate close


    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.get:{

                if(CheckInternet.getInstance().checkInternet(activity)){
                    new getRecords().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "http://digital.cygnismedia.com/ready-vax/public/api/v1/log/list");
                }else{
                    myMethods.showToast(activity, "Please connect internet");
                }

                break;
            }

            case R.id.post:{

                if(CheckInternet.getInstance().checkInternet(activity)){
                   new setUser().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[]{"http://digital.cygnismedia.com/ready-vax/public/api/v1/user/login", getPostParameters()});
                }else{
                    myMethods.showToast(activity, "Please connect internet");
                }

                break;
            }

        }
        //switch close
    }
    //Onclick listner end


    //Async start
    private class getRecords extends AsyncTask<String, Void, String> {
        String status = "", response = "";
        JSONObject object;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Please wait!");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String[] header = {"Content-Type", "application/json", "Authorization", "AccessToken"};
            response = WebService.getInstance().getResponse(params[0], header, activity);

            if (response.equals("errorConnection")) {


            } else {
                try {

                    object = new JSONObject(response);
                    status = object.toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return status;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();

            myMethods.showToast(activity, ""+result);

        }

    }
    //Async close


    private String getPostParameters() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", "talibjaml75@gmail.com");
            jsonObject.put("password", "sdfsafdsdf");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();

    }



    private class setUser extends AsyncTask<String, Void, String> {

        String status = "", response = "";

        JSONObject resObj;
        JSONObject dataObj;
        JSONArray messages;
        String code = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Please wait!");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            String[] headerValue = {"Content-Type", "application/json"};
            HashMap<String, String> builderResponse = WebService.getInstance().postJsonResponse(params[0], params[1], headerValue, getApplicationContext());

            status = builderResponse.get("status");

            if (status.equals("errorConnection"))

                response = "Not internet connectivity found";

            else if (status.equals("error")) {

                response = builderResponse.get("data");

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    status = jsonObject.toString();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                response = builderResponse.get("data");

                try {
                    JSONObject jsonObject = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                    response = "error";

                }




            }

            return status;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            myMethods.showToast(activity, ""+result);
        }
    }




}
