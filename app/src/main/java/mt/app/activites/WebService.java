package mt.app.activites;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.ClientProtocolException;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

/**
 * Created by talib on 10/19/2016.
 */

public class WebService {
    static WebService webservice;

    public static WebService getInstance() {

        if (webservice == null)
            webservice = new WebService();

        return webservice;
    }

    public String getResponse(String postUrl, String[] header, Context context) {
        Log.d("url", postUrl);
        int statusCode;

        StringBuilder builder = new StringBuilder();
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(postUrl);

        //adding header
        int size = header.length;
        if (size > 0) {

            for (int i = 0; i < size; i += 2) {
                httpget.addHeader(header[i], header[i + 1]);
            }

        }

        try {

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httpget);
            StatusLine statusLine = response.getStatusLine();
            statusCode = statusLine.getStatusCode();

            if (statusCode == 401 || statusCode == 400) {

                String ResponseToReturn = "";

                String[] header_ = {"Content-Type", "application/json", "Authorization", "Access Token"};
                ResponseToReturn = getResponse(postUrl, header_, context);


                return ResponseToReturn;

            } else {

                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content), 8192);
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }


            }


        } catch (ClientProtocolException e) {

        } catch (IOException e) {
            return String.valueOf("errorConnection");
        }
        // Log.d("testting", "testingbuilder" + builder.toString());

        return builder.toString();
    }
    //get json response


    /* postJsonResponse */
    public HashMap<String, String> postJsonResponse(String url, String jsonResponse, String[] header, Context context) {
        HashMap<String, String> builderResponseArray;
        int statusCode = 0;
        builderResponseArray = new HashMap<String, String>();


            StringBuilder builder = new StringBuilder();
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);
            try {


                httppost.setEntity(new StringEntity(jsonResponse, "UTF-8"));
                int size = header.length;
                if (size > 0) {

                    for (int i = 0; i < size; i += 2) {
                        httppost.addHeader(header[i], header[i + 1]);

                    }
                }


                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                statusCode = statusLine.getStatusCode();

                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                if (statusCode == 200) {

                    builderResponseArray.put("status", "success");
                    builderResponseArray.put("code", String.valueOf(statusCode));
                    builderResponseArray.put("data", builder.toString());

                } else {

                    builderResponseArray.put("status", "error");
                    builderResponseArray.put("code", String.valueOf(statusCode));
                    builderResponseArray.put("data", builder.toString());

                }


            } catch (ClientProtocolException e) {
            } catch (IOException e) {

                builderResponseArray.put("status", "errorConnection");
                builderResponseArray.put("code", String.valueOf(statusCode));
                builderResponseArray.put("data", "");

            } catch (Exception ex) {

            }



        return builderResponseArray;
    }
     /* postJsonResponse  end*/


}
