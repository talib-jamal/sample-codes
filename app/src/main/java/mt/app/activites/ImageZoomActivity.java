package mt.app.activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import mt.app.R;
import mt.app.imagzoom.ImageZoom;

public class ImageZoomActivity extends AppCompatActivity {

    ImageZoom image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_zoom);


        image = (ImageZoom) findViewById(R.id.image);
        image.setImageResource(R.drawable.ic_launcher);
        image.isActivated();

    }
}
