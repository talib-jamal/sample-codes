package mt.app;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import mt.app.basicfunctions.basic.MyMethods;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText search;
    private LinearLayout iconSearchLayout;
    private ImageView actionSearchIcon;
    private Activity activity;
    private MyMethods myMethods;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_main);

        init();


    }
    //oncreate close

    //initializing views start
    public void init(){
        activity = this;

        getToolBar();

        search = (EditText)findViewById(R.id.search);
        iconSearchLayout = (LinearLayout)findViewById(R.id.iconSearchLayout);
        iconSearchLayout.setOnClickListener(this);

        actionSearchIcon = (ImageView)findViewById(R.id.actionSearchIcon);



    }
    //initializing views end

    //get toolbar start
    public void getToolBar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Sample Codes");

    }
    //get toolbar end


    @Override
    public void onClick(View v) {

        switch(v.getId()){

            case R.id.iconSearchLayout:{

                if(actionSearchIcon.getTag().toString().equals("search")){
                    actionSearchIcon.setTag("cancel");
                    actionSearchIcon.setBackgroundResource(R.drawable.ic_cancel_black_24dp);

                }else{
                    if(actionSearchIcon.getTag().toString().equals("cancel")){
                        actionSearchIcon.setTag("search");
                        actionSearchIcon.setBackgroundResource(R.drawable.ic_search_black_24dp);
                    }
                }

                break;
            }

        }

    }
}
