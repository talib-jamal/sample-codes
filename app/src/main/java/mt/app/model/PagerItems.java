package mt.app.model;

/**
 * Created by talib on 10/19/2016.
 */

//Pager item class start
public  class PagerItems {
    private final CharSequence mTitle;
    private final int mIosIconResId;
    private final int mAndroidIconResId;
    private final int mIndicatorColor;
    private final int mDividerColor;

    public PagerItems(CharSequence title, int iosIconResId, int androidIconResId, int indicatorColor, int dividerColor) {
        mTitle = title;
        mIosIconResId = iosIconResId;
        mAndroidIconResId = androidIconResId;
        mIndicatorColor = indicatorColor;
        mDividerColor = dividerColor;
    }


    public CharSequence getTitle() {
        return mTitle;
    }

    public int getIosIconResId() {
        return mIosIconResId;
    }

    public int getAndroidIconResId() {
        return mAndroidIconResId;
    }

    public int getIndicatorColor() {
        return mIndicatorColor;
    }

    public int getDividerColor() {
        return mDividerColor;
    }


}
//Pager Item close