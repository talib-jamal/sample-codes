package mt.app.basicfunctions.SharedPreference;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Talib on 8/28/2015.
 */

public class SaveInSharedPreference {


   static SaveInSharedPreference savePrefs;


    public static SaveInSharedPreference getInstance() {

        if(savePrefs == null){
            savePrefs = new SaveInSharedPreference();
        }

        return savePrefs;
    }


    //making saveData method start
    public void saveData(Activity act, String key, boolean data) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(act);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, data);
        editor.commit();
    }
    //making saveData method end

    //get getBoolean method start
    public static boolean getBoolean(Activity act, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(act);
        boolean value = prefs.getBoolean(key, false);
        return value;
    }
    //get getBoolean method end


    //making saveData method start
    public void saveData(Activity act, String key, String data) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(act);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, data);
        editor.commit();
    }
    //making saveData method end


    //get getValues method start
    public static String getValues(Activity act, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(act);
        String value = prefs.getString(key, "");
        return value;
    }
    //get getValues method end

    //get clear key method start
    public static void clearKey( Activity act, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(act);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }
    //get clear key method end
}
