package mt.app.basicfunctions.basic;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import mt.app.R;
import mt.app.basicfunctions.SharedPreference.SaveInSharedPreference;

/**
 * Created by Talib on 8/28/2015.
 */
public class MyMethods {

    public static Drawable drawableValid, drawableInValid;
    SaveInSharedPreference savePrefs;
    static Typeface face;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
    String emailPattern2 = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    static MyMethods myMethods;
    String lastWord = "";


    public static MyMethods getInsance() {

        if (myMethods == null) {
            myMethods = new MyMethods();
        }

        return myMethods;
    }


    public static Typeface getFont(Context context, String fontName) {
        return face = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
    }


    public void fullScreen(Activity act) {
        act.requestWindowFeature(Window.FEATURE_NO_TITLE);
        act.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public void checkEditText(final EditText edit) {
        edit.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                if (count != 0) {
                    edit.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableValid, null);
                } else {
                    edit.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });
    }

    //Verifying Email Pattern
    private boolean emailValidation(EditText email) {

        if (email.getText().toString().trim().matches(emailPattern) || email.getText().toString().trim().matches(emailPattern2)) {
            return true;
        } else {
            email.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableInValid, null);
            return false;
        }
    }
    //Verifying Email Pattern End


    public void checkEditTextEmail(final EditText edit) {
        edit.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

                if (count != 0) {

                    if (emailValidation(edit)) {
                        edit.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableValid, null);
                    } else {
                        edit.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableInValid, null);
                    }

                } else {
                    edit.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }

        });
    }


    public void textWatcherCapitalWord(final EditText et1) {

        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence et, int start, int before, int count) {

                String s = et.toString();
                StringBuilder sb = new StringBuilder(s);

                if (et.toString().length() > 0){

                    et1.setBackgroundResource(R.drawable.edit_valid);

                    if (!s.equals(s.toUpperCase())) {

                        if (et.toString().length() == 1) {
                            sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
                            et1.setText(sb.toString());
                            et1.setSelection(et1.getText().length());
                        } else {

                            if (lastWord.equals(" ")) {

                                lastWord = String.valueOf(s.charAt(s.length() - 1));

                                int i = 0;
                                for (int m = 0; m < s.length(); m++) {
                                    if (String.valueOf(s.charAt(m)).equals(" ")) {
                                        i = m + 1;
                                    }
                                }

                                sb.setCharAt(i, Character.toUpperCase(sb.charAt(i)));
                                et1.setText(sb.toString());
                                et1.setSelection(et1.getText().length());

                            }

                        }


                    }

                    if (s.length() != 0) {
                        lastWord = String.valueOf(s.charAt(s.length() - 1));
                    }

                }






            }

            @Override
            public void afterTextChanged(Editable et) {

            }
        });
    }


    public void showToast(Activity act, String msg) {
        Toast.makeText(act, msg, Toast.LENGTH_SHORT).show();
    }

    //Validation on EditText Start

    //Hide edit text start
    public static boolean hide(EditText field, InputMethodManager imm) {

        imm.hideSoftInputFromWindow(field.getApplicationWindowToken(), 0);

        return false;

    }
    //Hide Edit text end

    //get date start
    public static String getDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy");

        return sdf.format(cal.getTime());
    }
    //get date end

    //Encode Bitmap into String start
    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);

        return imageEncoded;
    }
    //Encode Bitmap into String end


    //Decode Imgae from String to Bitmap Start
    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }
    //Decode Imgae from String to Bitmap End


    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }


    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }


    public void transitionAlpha(LinearLayout layout, int duration) {
        ObjectAnimator.ofFloat(layout, "alpha", 1, 0, 1).setDuration(duration).start();
    }

    public int getDimensionW(Activity act) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    public int getDimensionH(Activity act) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }


    // Show Alert Box Start
    public void resultAlert(final Activity act, String title, String msg, final String close) {

        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle(title);
        builder.setMessage(msg);

        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if (close.equals("1")) {
                    act.finish();
                }

            }
        });

        builder.show();
    }


    public static boolean CheckIfAppRunning(Context ctx) {
        boolean isoppened = false;
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(ctx.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals("com.app.readyvax")) {

                isoppened = true;
                break;
            }

        }
        return isoppened;
    }


}
