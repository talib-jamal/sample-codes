package mt.app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Talib on 8/28/2015.
 */

public class CheckInternet {

	static ConnectivityManager connectivity;
	static NetworkInfo activeNetwork;
	static NetworkInfo mobileNetworkInfo;
	static CheckInternet checkInternet;
	static boolean check = false;

	public static CheckInternet getInstance(){

		if(checkInternet == null){
			checkInternet = new CheckInternet();
		}
		return checkInternet;
	}
	
	
	public static boolean checkInternet(Context context){

		connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		activeNetwork = connectivity.getActiveNetworkInfo();


		if (activeNetwork != null) {

			if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI){
				check = true;
			}else{
				if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
//					Toast.makeText(context, activeNetwork.getTypeName(), Toast.LENGTH_SHORT).show();
					check = true;
				}
			}


			return check;
		}else{
			return false;
		}


		
	}


	public boolean isConnectingToInternet(Context context) {
		boolean connected = false;
		connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		activeNetwork = connectivity.getActiveNetworkInfo();

		if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
			try {
				if (InetAddress.getByName("google.com").isReachable(7000)) {
					// host reachable
					connected = true;
				} else {
					connected = false;
//                    connected = false;
					// host not reachable
				}
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return connected;
	}

}
