package mt.app.background;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * Created by talib on 10/6/2016.
 */
public class MyService extends Service {

    /** indicates how to behave if the service is killed */
    int mStartMode;

    /** interface for clients that bind */
    IBinder mBinder;

    /** indicates whether onRebind should be used */
    boolean mAllowRebind;

    public static final String NOTIFICATION = "cygnis.background.background.receiver";

    private Handler handler;
    private long initial_time;
    long timeInMilliseconds = 0L;
    int timer = 0;

    /** Called when the service is being created. */
    @Override
    public void onCreate() {
        handler = new Handler();
        initial_time = SystemClock.uptimeMillis();
        handler.removeCallbacks(sendUpdatesToUI);
        handler.postDelayed(sendUpdatesToUI, 1000); // 1 second
    }

    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
            DisplayLoggingInfo();
            handler.postDelayed(this, 1000); // 1 seconds
        }
    };

    private void DisplayLoggingInfo() {

        timeInMilliseconds = SystemClock.uptimeMillis() - initial_time;

        timer = (int) timeInMilliseconds / 1000;
        //Toast.makeText(getApplicationContext(), ""+timer, Toast.LENGTH_LONG).show();

    }


    /** The service is starting, due to a call to startService() */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       // Toast.makeText(getApplicationContext(), "Service Start", Toast.LENGTH_SHORT).show();

        return mStartMode;
    }

    /** A client is binding to the service with bindService() */
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /** Called when all clients have unbound with unbindService() */
    @Override
    public boolean onUnbind(Intent intent) {
        return mAllowRebind;
    }

    /** Called when a client is binding to the service with bindService()*/
    @Override
    public void onRebind(Intent intent) {

    }

    /** Called when The service is no longer used and is being destroyed */
    @Override
    public void onDestroy() {
        //Toast.makeText(getApplicationContext(), "Service Stop", Toast.LENGTH_SHORT).show();
        handler.removeCallbacks(sendUpdatesToUI);

        /*SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("timer", timer);
        editor.commit();
*/
       // BackgroundActivity.updateUI(timer);

        Intent intent = new Intent();
        intent.setAction(NOTIFICATION);
        intent.putExtra("timer", timer);
        sendBroadcast(intent);


    }




}