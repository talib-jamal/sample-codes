package mt.app.background;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import mt.app.R;


public class FourthActivity extends AppCompatActivity  implements MyActivityLifecycleCallbacks.SetTime{

    Button send;
    RelativeLayout activity_main2;
    TextView timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        activity_main2 = (RelativeLayout)findViewById(R.id.activity_main2);
        activity_main2.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        timer = (TextView)findViewById(R.id.timer);
        timer.setText("Fourth Activity");

        send = (Button)findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });


    }

    @Override
    public void setTime(int time) {
        timer.setText("Sec: "+time);
    }


}
