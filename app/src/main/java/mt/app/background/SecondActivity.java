package mt.app.background;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mt.app.R;


public class SecondActivity extends AppCompatActivity implements MyActivityLifecycleCallbacks.SetTime{

    Button send;
    TextView timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        timer = (TextView)findViewById(R.id.timer);

        send = (Button)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(SecondActivity.this, ThirdActivity.class);
                startActivity(intent);
            }
        });


    }


    @Override
    public void setTime(int time) {
        timer.setText("Sec: "+time);
    }

}
