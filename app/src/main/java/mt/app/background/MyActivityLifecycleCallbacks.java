package mt.app.background;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by talib on 10/24/2016.
 */

public class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    Activity activity;

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        Log.i(activity.getClass().getSimpleName(), "onCreate(Bundle)");
        //Toast.makeText(activity,"Activity Created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onStart()");
        //Toast.makeText(activity,"Activity Started", Toast.LENGTH_SHORT).show();
        activity.stopService(new Intent(activity.getBaseContext(), MyService.class));
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onResume()");
        //Toast.makeText(activity,"Activity Resume", Toast.LENGTH_SHORT).show();



    }

    @Override
    public void onActivityPaused(Activity activity) {
        this.activity = activity;

        //Toast.makeText(activity,"Activity Paused", Toast.LENGTH_SHORT).show();
        Log.i(activity.getClass().getSimpleName(), "onPause()");

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyService.NOTIFICATION);
        activity.registerReceiver(receiver , intentFilter);

        //Start our own service
        Intent intent = new Intent(activity, MyService.class);
        activity.startService(intent);

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.i(activity.getClass().getSimpleName(), "onSaveInstanceState(Bundle)");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onStop()");
       // Toast.makeText(activity,"Activity Stoped", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onDestroy()");

        try {
            if (receiver != null) {
                activity.unregisterReceiver(receiver);
            }
        } catch (IllegalArgumentException e) {

        }

        Intent intent = new Intent(activity, MyService.class);
        activity.stopService(intent);

    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();


            if (bundle != null) {
                //updateUI(intent.getIntExtra("timer", 0));
               // unregisterReceiver(receiver);
                //Toast.makeText(activity,""+intent.getIntExtra("timer", 0), Toast.LENGTH_SHORT).show();
               // activity.unregisterReceiver(receiver);

                SetTime setTime = (SetTime) activity;
                setTime.setTime(intent.getIntExtra("timer", 0));

            }
        }
    };


    interface SetTime{
        void setTime(int time);
    }


}
