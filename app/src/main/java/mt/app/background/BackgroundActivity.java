package mt.app.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import mt.app.R;


public class BackgroundActivity extends AppCompatActivity implements MyActivityLifecycleCallbacks.SetTime{

    static TextView timer;
    private final MyActivityLifecycleCallbacks mCallbacks = new MyActivityLifecycleCallbacks();
    Button send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background);

        getApplication().registerActivityLifecycleCallbacks(mCallbacks);


       // Toast.makeText(this ,"Activity Created", Toast.LENGTH_SHORT).show();

        timer = (TextView)findViewById(R.id.timer);
        send = (Button)findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(BackgroundActivity.this, SecondActivity.class);
                startActivity(intent);
            }
        });




    }

    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this ,"Activity Resume", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
       // Toast.makeText(this ,"Activity Restart", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStop() {
        super.onStop();
       // Toast.makeText(this ,"Activity Stop", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
       // Toast.makeText(this ,"Activity Paused", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Toast.makeText(this ,"Activity Start", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Toast.makeText(this ,"Activity Destory", Toast.LENGTH_SHORT).show();


    }


    @Override
    public void setTime(int time) {
        timer.setText("Sec: "+time);
    }
}
