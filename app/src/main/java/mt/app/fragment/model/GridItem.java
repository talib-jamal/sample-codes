package mt.app.fragment.model;

/**
 * Created by Cygnismedia on 10/13/2016.
 */

public class GridItem {

    private String url;

    public GridItem(String url){
        setUrl(url);
    }

    public void setUrl(String urls){
        this.url = urls;
    }

    public String getUrl(){
        return url;
    }



}
