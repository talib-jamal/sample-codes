package mt.app.fragment.model;

import android.animation.TimeInterpolator;

public class ItemModel {
    public final String description, id, name;
    public final int colorId1;
    public final int colorId2;
    public final TimeInterpolator interpolator;

    public ItemModel(String id , String name ,String description, int colorId1, int colorId2, TimeInterpolator interpolator) {
        this.id = name;
        this.name = name;
        this.description = description;
        this.colorId1 = colorId1;
        this.colorId2 = colorId2;
        this.interpolator = interpolator;
    }
}