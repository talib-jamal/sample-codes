package mt.app.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import mt.app.CheckInternet;
import mt.app.DividerItemDecoration;
import mt.app.R;
import mt.app.expandablelayout.Utils;
import mt.app.fragment.adapter.RecyclerViewRecyclerAdapter;
import mt.app.fragment.model.ItemModel;


public class ThirdFragment extends Fragment {

    private View parentView = null;
    private RecyclerView recyclerView;
    private Activity activity;
    private ProgressDialog prgDialog;
    private CheckInternet checkInternet;
    List<ItemModel> data ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(parentView == null) {
            parentView = inflater.inflate(R.layout.third, container, false);

            setUpViews();

        }

        return parentView;
    }


    //setup views start
    private void setUpViews() {

        activity = getActivity();
        FragmentActivity parentActivity = (FragmentActivity) getActivity();
        checkInternet = CheckInternet.getInstance();
        data = new ArrayList<>();

        recyclerView = (RecyclerView) parentView.findViewById(R.id.recycler_view);
        recyclerView.addItemDecoration(new DividerItemDecoration(activity));
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));

        if(checkInternet.checkInternet(activity)){
            gettingData();
        }else{
            Toast.makeText(activity, "Please connect with internet", Toast.LENGTH_SHORT).show();
        }


    }
    //setup views end




    //set data start
    public void setAdapter(List<ItemModel> arrayData){
        recyclerView.setAdapter(new RecyclerViewRecyclerAdapter(arrayData));
    }

    //set data close


    //getting data Start
    public void gettingData() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://digital.cygnismedia.com/ready-vax/public/api/v1/vaccine/list?pagination=0", new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started

                prgDialog = new ProgressDialog(activity);
                prgDialog.setTitle("Please wait!");
                prgDialog.setMessage("Getting data");
                prgDialog.show();
                prgDialog.setCancelable(false);

            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                prgDialog.dismiss();
                Toast.makeText(activity, "Internet is not responding!", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(int arg0, Header[] arg1, byte[] arg2,
                                  Throwable arg3) {
                // TODO Auto-generated method stub
                prgDialog.dismiss();
                Toast.makeText(activity, "Internet is not responding!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                // TODO Auto-generated method stub

                if (statusCode == 200) {

                    String s = new String(response);
                    DownloadText(s);

                }

            }
        });


    }
    //getting data End
    private void DownloadText(String result) {

        JSONObject json = null;
        String results = "";

        try {

            json = new JSONObject(result);

            JSONArray jsonArrayData = json.getJSONArray("data");
            if(jsonArrayData.length() !=0 ){


                for (int j = 0; j <jsonArrayData.length(); j++) {
                    JSONObject jsonObjData = jsonArrayData.getJSONObject(j);
                    data.add(new ItemModel(
                            jsonObjData.optString("id"),
                            jsonObjData.optString("name"),
                            jsonObjData.optString("detail"),
                            R.color.material_blue_300,
                            R.color.material_blue_400,
                            Utils.createInterpolator(Utils.ACCELERATE_DECELERATE_INTERPOLATOR)));
                }

                setAdapter(data);

            }else{
                Toast.makeText(activity, "Data not found!", Toast.LENGTH_SHORT).show();
            }

            prgDialog.dismiss();
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            prgDialog.dismiss();
            e1.printStackTrace();
            Toast.makeText(activity, "Something went wrong!", Toast.LENGTH_SHORT).show();
        }





    }
    //Download text close



}
