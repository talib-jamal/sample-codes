package mt.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mt.app.DividerItemDecoration;
import mt.app.R;
import mt.app.fragment.adapter.GridAdapter;
import mt.app.fragment.model.GridItem;


public class SecondFragment extends Fragment {

    private View parentView = null;
    private RecyclerView recyclerView;
    private GridAdapter gridAdapter;
    private List<GridItem> arrayList;
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.second, container, false);


            setUpViews();
            // getAdapter();
        }

        return parentView;
    }


    //setup views start
    private void setUpViews() {
        activity = getActivity();

        FragmentActivity parentActivity = (FragmentActivity) getActivity();

        recyclerView = (RecyclerView) parentView.findViewById(R.id.recycler_view);
        arrayList = new ArrayList<GridItem>();

        storeData();

        gridAdapter = new GridAdapter(arrayList, getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(activity));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(gridAdapter);


    }
    //setup views end

    //Store data start
    private void storeData() {

        if (arrayList.size() > 0) {
            arrayList.clear();
        }

        arrayList.add(new GridItem("https://futurestud.io/images/futurestudio-logo-transparent.png"));
        arrayList.add(new GridItem("https://www-assets-cdn.withings.com/back//media/partner-app/retrofit/logo.png"));
        arrayList.add(new GridItem("http://blog.robinchutaux.com/images/logo.png"));

        arrayList.add(new GridItem("https://futurestud.io/images/futurestudio-logo-transparent.png"));
        arrayList.add(new GridItem("https://www-assets-cdn.withings.com/back//media/partner-app/retrofit/logo.png"));
        arrayList.add(new GridItem("http://blog.robinchutaux.com/images/logo.png"));
    }
    //store data end


}
