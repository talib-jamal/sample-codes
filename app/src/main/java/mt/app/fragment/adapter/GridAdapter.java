package mt.app.fragment.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.List;

import mt.app.R;
import mt.app.fragment.model.GridItem;

/**
 * Created by Talib on 10/13/2016.
 */

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.MyViewHolder> {

    private List<GridItem> gridItemList;
    private Activity activity;
    private MyViewHolder holder;
    private  GridItem item;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);

            image = (ImageView) view.findViewById(R.id.image);
        }
    }


    public GridAdapter(List<GridItem> gridItems, Activity activity) {
        this.gridItemList = gridItems;
        this.activity = activity;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid, parent, false);

        holder = new MyViewHolder(itemView);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        item = gridItemList.get(position);

        Glide.with(activity)
                .load(item.getUrl())
                .asBitmap()
                .placeholder(R.drawable.ic_launcher)
                .into(target);

    }

    private SimpleTarget target = new SimpleTarget<Bitmap>() {
        @Override
        public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {

            holder.image.setBackgroundResource(0);
            holder.image.setImageBitmap(bitmap);

        }
    };


    @Override
    public int getItemCount() {
        return gridItemList.size();
    }
}