package mt.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import mt.app.R;
import mt.app.drawer.DrawerItemClick;
import mt.app.drawer.FragmentDrawer;

public class FragmentActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener{

    private FragmentDrawer drawerFragment;
    private Activity activity;
    private DrawerItemClick drawerItemClick;
    private Toolbar mToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);



        getToolBar();
        getDrawer();

        changeFragment(new HomeFragment(), "Gridlayout");

    }
    //onCreate Close

    //Change fragment start
    private void changeFragment(Fragment targetFragment, String text){

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();


        getSupportActionBar().setTitle(text);

    }
    //change fragment close


    //Get ToolBar start
    public void getToolBar(){

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Home");

    }
    //Get ToolBar end

    //Get Drawer start
    public void getDrawer(){
        drawerFragment = (FragmentDrawer)getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
    }
    //Get Drawer end



    //Drawer item listener start
    @Override
    public void onDrawerItemSelected(View view, int position) {

        switch(position){

            case 0:{
                changeFragment(new HomeFragment(), "Gridlayout");
                break;
            }

            case 1:{
                changeFragment(new SecondFragment(), "Linearlayout");
                break;
            }

            case 2:{
                changeFragment(new ThirdFragment(), "Expandablelayout");
                break;
            }

        }

    }
    //Drawer item listener end
}
