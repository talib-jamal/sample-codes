package mt.app.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import mt.app.DividerItemDecoration;
import mt.app.R;
import mt.app.fragment.adapter.GridAdapter;
import mt.app.fragment.model.GridItem;


public class CameraFragment extends Fragment {

    private View parentView = null;
    private RecyclerView recyclerView;
    private GridAdapter gridAdapter;
    private List<GridItem> arrayList;
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (parentView == null) {
            parentView = inflater.inflate(R.layout.camera_fragment, container, false);


            setUpViews();
            // getAdapter();
        }

        return parentView;
    }


    //setup views start
    private void setUpViews() {

        activity = getActivity();

        //FragmentActivity parentActivity = (FragmentActivity) getActivity();



    }
    //setup views end




}
